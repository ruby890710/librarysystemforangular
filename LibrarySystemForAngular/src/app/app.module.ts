import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LibrarySystemModule } from './library-system/library-system.module';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LibrarySystemModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
