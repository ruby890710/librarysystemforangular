import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'bookFilter',
    pure: false
})

export class BookFilterPipe implements PipeTransform {
    transform(tableData: any[], searchText: string): any {
        // 如果searchText有值，則對關鍵字搜尋
        if (searchText) {
            let isContainSearchText: boolean = false;
            let result = tableData.filter(row => {
                isContainSearchText = false;
                // 比對每一列每一欄是否包含searchText
                Object.keys(row).forEach((col: any) => {
                    if (row[col].toString().toLowerCase().includes(searchText.toLowerCase())) {
                        isContainSearchText = true;
                    }
                })
                // 回傳該列是否包含searchText之布林值
                return isContainSearchText;
            });
            return result;
        }
        else {
            return tableData;
        }
    }
}
