import { Component, OnInit } from '@angular/core';
import { Book } from './book-model';
declare var bookData: any;

@Component({
    selector: 'app-library-system',
    templateUrl: './library-system.component.html',
    styleUrls: ['./library-system.component.css']
})

export class LibrarySystemComponent implements OnInit {
    // 建立書籍vielModel
    viewModel: Book = new Book();
    // 書籍資料之table欄位
    public tableColumnNames: Array<string> = [
        '書籍編號', '書籍類別', '書籍名稱', '書籍作者', '購買日期'
    ];
    // 搜尋書籍輸入值
    public searchText: string = '';
    // 書籍新增view是否開啟
    public isOpenInsertView: boolean = false;
    // 書籍類別圖片路徑
    public imgPath: string = 'assets/images/database.jpg';
    // 書籍類別dropdownlist資料
    public bookCategoryData = [
        // value設定為各書籍類別的圖片名稱
        { text: '資料庫', value: 'database' },
        { text: '網際網路', value: 'internet' },
        { text: '應用系統整合', value: 'system' },
        { text: '家庭保健', value: 'home' },
        { text: '語言', value: 'language' }
    ];
    // table內資料
    public tableData: any = bookData;

    constructor() {
    }

    ngOnInit(): void {
        this.initInsertView()
        this.loadBookData();
    }

    // 初始化新增view
    public initInsertView() {
        this.viewModel.book_category = { text: '資料庫', value: 'database' };
        this.viewModel.book_name = '';
        this.viewModel.book_author = '';
        this.viewModel.book_bought_date = '';
    }

    // 載入書籍資料
    public bookDataFromLocalStorage: any = [];
    public loadBookData() {
        // getItem回傳型態string || null，null不可轉成json
        let data = localStorage.getItem('bookData') || ''
        if (data === '') {
            this.bookDataFromLocalStorage = bookData;
            localStorage.setItem('bookData', JSON.stringify(this.bookDataFromLocalStorage));
        }
        else {
            this.bookDataFromLocalStorage = JSON.parse(data);
        }
        this.tableData = this.bookDataFromLocalStorage;
    }

    // 開啟新增view
    public openInsertView() {
        this.isOpenInsertView = true;
    }

    // 設定書籍類別dropdownlist、書籍類別圖片初始值
    compareFn = this._compareFn.bind(this);
    // a為dropdownlist所有值，b為ngModel值
    _compareFn(a: any, b: any) {
        // 設定初始類別圖片
        if (a && b) {
            this.imgPath = 'assets/images/' + b.value + '.jpg';
        }
        // 沒有用FormControl時compare方式
        return a && b ? a.value === b.value : a === b;
    }

    // 書籍類別變動時，變更類別圖片
    public onBookCategoryChange(event: any) {
        this.imgPath = 'assets/images/' + (event.value) + '.jpg';
    }

    // 新增書籍
    public bookIdInData: Array<number> = [];
    public insertBook() {
        // 新增書籍的BookId = localStorage最大的BookId+1 -> item宣告
        let maxBookIdInData = Math.max.apply(Math, this.bookDataFromLocalStorage.map((o: any) => { return o.BookId; }))
        let bookId: number = (maxBookIdInData + 1);
        // 新書籍資料
        let newBook = {
            'BookId': bookId,
            'BookCategory': this.viewModel.book_category.text,
            'BookName': this.viewModel.book_name,
            'BookAuthor': this.viewModel.book_author,
            'BookBoughtDate': this.viewModel.book_bought_date,
            'BookPublisher': 'Ruby'
        }

        // 新增書籍
        this.bookDataFromLocalStorage.push(newBook);
        localStorage.setItem('bookData', JSON.stringify(this.bookDataFromLocalStorage));
        this.tableData = this.bookDataFromLocalStorage;
        // 顯示新增成功提示
        alert('已新增' + this.displayChangeBookData(newBook));
        // 重置新增view內容
        this.initInsertView();
    }

    // 取消->關閉新增書籍view
    public closeInsertView() {
        this.isOpenInsertView = false;
        this.initInsertView();
    }

    // 刪除選取列
    deleteSelectedBook(book: any) {
        if (confirm('是否刪除' + this.displayChangeBookData(book))) {
            // 刪除table選取列
            for (let i = 0; i < this.tableData.length; ++i) {
                if (this.tableData[i].BookId === book.BookId) {
                    this.tableData.splice(i, 1);
                }
            }
            this.bookDataFromLocalStorage = this.tableData;
            localStorage.setItem('bookData', JSON.stringify(this.bookDataFromLocalStorage));
            alert('已刪除：(' + book.BookId + ') ' + book.BookName);
        }
    }

    // 資料變動時顯示資料
    public displayChangeBookData(book: any) {
        return ('\n書籍編號：(' + book.BookId + ')\n書籍類別：' + book.BookCategory
            + '\n書籍名稱：' + book.BookName + '\n書籍作者：' + book.BookAuthor + '\n購買日期：' + book.BookBoughtDate)
    }
}
