import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibrarySystemComponent } from './library-system.component';
import { FormsModule, } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BookFilterPipe } from './book-filter-pipe';

@NgModule({
    declarations: [
        LibrarySystemComponent,
        BookFilterPipe
    ],
    imports: [
        CommonModule,
        FormsModule,
        BrowserModule
    ],
    exports: [
        LibrarySystemComponent,
        BookFilterPipe
    ]
})
export class LibrarySystemModule { }
