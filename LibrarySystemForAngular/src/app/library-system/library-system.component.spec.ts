import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LibrarySystemComponent } from './library-system.component';

describe('LibrarySystemComponent', () => {
  let component: LibrarySystemComponent;
  let fixture: ComponentFixture<LibrarySystemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LibrarySystemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LibrarySystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
