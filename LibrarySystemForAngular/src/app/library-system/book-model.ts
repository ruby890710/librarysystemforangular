export class Book {
    // 書籍類別為any型態，否則會顯示->類型 'object' 沒有屬性 'text'
    book_category: any = { text: '', value: '' };
    book_name: string = '';
    book_author: string = '';
    book_bought_date: string = '';
}
